-- 1
-- a)
{-
sq :: Int -> Int
sq x = x * x

sqList1 [] 		= []
sqList1 (x:xs) 	= sq x: sqList1 xs

sqList1HO x = map (^2) x
sqList1HO' x = take x (map(^2) [1..])

-- b)
sqList2 []		= []
sqList2 (x:xs)	= (x, sq x): sqList2 xs
sqList2HO x = zip x (map (^2) x)
sqList2HO' x = take x (zip [1..] (map(^2) [1..]))

-- c)
powList1 :: Int -> [Int]
powList1 x =
    if	x < 10^10	then (x: powList1 (x*3)) else []

-- d)
powList2HO x = take x (iterate (* 3) 1)

-- e)
mul3or5 :: Integer -> Bool
mul3or5 y
    | ((y `mod` 3) == 0) = True
    | ((y `mod` 5) == 0) = True
    | True               = False

sumMul :: Integer -> Integer
sumMul x = sum (filter mul3or5 [1..x])

-- g)
geoAux :: Int -> Int -> [Int]
geoAux x r = map (^r) [1..x]

geoPower :: Int -> Int -> [[Int]]
geoPower x y = map (geoAux x) [1..y]

-- h)
geoAux2 :: Int -> Int -> [Int]
geoAux2 y x = take y (iterate (*x) x)

geoPower2 :: Integer -> Integer -> [[Integer]]
geoPower2 x y = map (geoAux2 y) [1..x]
-}
-- i)
divisor :: Integer -> Integer -> Bool
divisor x y = (x `mod` y) == 0

divisores :: Integer -> [Integer]
divisores x = filter (divisor x) [1..x `div` 2]

primo :: Integer -> Bool
primo x = length (divisores x) == 1

listaPrimos :: Integer -> [Integer]
listaPrimos x = takeWhile (x >= ) (filter (primo) [1..])

-- j)
{-
media :: Num a => [Integer] -> a
media z = foldl (\x y -> fromIntegral (x + y) `div` fromIntegral 2) z
-}
media :: Fractional a => [Integer] -> a
media x = (fromIntegral (sum x)) / (fromIntegral (length x))

mediaPrimos :: Fractional a => Integer -> a
mediaPrimos x = media (listaPrimos x)

-- k)
listaPrimos' :: Integer -> Integer -> [Integer]
listaPrimos' mi ma = filter (primo) [mi..ma]

-- l)
listaPrimos'' :: Integer -> Integer
listaPrimos'' x = head (filter (x <= ) (filter (primo ) [1..]))

-- m)
maxFactorList :: Integer -> Integer -> [(Integer, [Integer])]
maxFactorList mi ma = zip [mi..ma] (map (divisores) [mi..ma])

-- n)
perfectList x = filter (\y -> (sum (divisores y)) == y) [1..x]

-- o)

-- 2
-- a)
filter2 :: Eq a => [a] -> (a -> Bool) -> (a -> Bool) -> ([a], [a])
filter2 xs p q = (filter p xs, filter q xs)
filter2Comp xs p q = [(x, y) | x <- xs, y <- xs, p x, q y]

-- b)
filtersAux :: Eq a => [a] -> (a -> Bool) -> [a]
filtersAux xs p = filter p xs

filters :: Eq a => [a] -> [(a -> Bool)] -> [[a]]
filters xs ps = map (filtersAux xs) ps

filtersComp :: Eq a => [a] -> [(a -> Bool)] -> [[a]]
filtersComp xs ps = [x | p <- ps, x <- [[y | y <- xs, p y]]]

-- c)
partAux p xs = filter p' xs
    where p' xs = not (p xs)

partition :: Eq a => (a -> Bool) -> [a] -> ([a], [a])
partition p xs = (filter p xs, partAux p xs)

partitionComp :: Eq a => (a -> Bool) -> [a] -> ([a], [a])
partitionComp p xs = ([x | x <- xs, p x], [y | y <- xs, p y == False])

-- d)
span :: Eq a => (a -> Bool) -> [a] -> ([a], [a])
span p xs = (takeWhile p xs, dropWhile p xs)

-- e) f x == g x, for all n <= x <= m
iguales :: (Enum a, Eq a, Eq t) => (a -> t) -> (a -> t) -> a -> a -> Bool
iguales f g n m =  length (filter (\x -> f x == g x) [n..m]) == length [n..m]

igualesComp :: (Enum a, Eq a, Eq t) => (a -> t) -> (a -> t) -> a -> a -> Bool
igualesComp f g n m = [x | x <- [n..m], f x == g x] == [n..m]

-- f) No. of elements of the list xs that fulfill p
cuantos :: (a -> Bool) -> [a] -> Int
cuantos p xs = length (filter p xs)

cuantosComp :: (a -> Bool) -> [a] -> Int
cuantosComp p xs = length [x | x <- xs, p x]

-- g) Most of the elements of xs fulfill p
mayoria :: (a -> Bool) -> [a] -> Bool
mayoria p xs = (length (filter p xs)) >= ((length xs) `div` 2)

-- h)
menorA :: (Enum a) => a -> a -> (a -> Bool) -> a
menorA n m p = head (filter (p) [n..m])

-- i)
menor :: (Enum a) => a -> (a -> Bool) -> a
menor n p = head (filter (p) [n..])
