-- 1
-- fullSeconds s = (show 2 s `div` 60)
-- fullMinutes s = (show fullMinutes s `div` 60)
-- fullHours h = (show fullDays h `div` 24)
-- fullDays d = (show fullYears d `div` 365)
-- fullYears y = show y
-- 	y = y `mod` 



--	(putStrLn "Years: ")
--	(show (t `div` 60  `div` 60 `div` 24 `div` 365))


-- 2
f x y = 2*x - y*x
g x = f (f 2 x) (f x 1)
h x y z = f (f (x + 2 * y) (g 3)) (5 - g(z) - y)
i x y 
	| x >= y && y > 0	= x - y
	| 0 < x && x < y 	= 0
	| True				= y - x