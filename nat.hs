data Nat = Cero | Suc Nat deriving (Show, Eq)

-- Auxiliares

fromInt:: Int -> Nat
fromInt 0 = Cero
fromInt x = Suc (fromInt (x-1))

fi = fromInt

toInt:: Nat -> Int
toInt Cero = 0
toInt (Suc x) = 1 + toInt x

ti = toInt

-- Operaciones

suma:: Nat -> Nat -> Nat
suma Cero y = y
suma (Suc Cero) y = (Suc y)
suma (Suc x) y = Suc(suma x y)

resta:: Nat -> Nat -> Nat
resta x Cero = x
resta (Suc x) (Suc y) = resta x y

multiplica:: Nat -> Nat -> Nat
multiplica Cero y = Cero
multiplica (Suc Cero) y = y
multiplica (Suc x) y = suma y (multiplica x y)

-- potencia x y = x^y
potencia:: Nat -> Nat -> Nat
potencia Cero y = Cero
potencia x Cero = (Suc Cero)
potencia x (Suc y) = multiplica x (potencia x y)