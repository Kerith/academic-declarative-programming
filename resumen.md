# PD temas

## General

- Not. Currificada: f a b c = (((f a) b) c)
- Orden de Evaluación de Ecuaciones

## Evaluación perezosa

- Definición de (&&) con Ev. perezosa
- Definición de 'bottom'

## Listas

- Tipos y listas

  - [[ `tail [1] == tail [True]` ]] = ?
  - [[ `:t []` ]] = ?
  - [[ `tail [3 div 0, 7]` ]] = ?
  - [[ `head []` ]] = ?
  - [[ `tail [1, tail["a"], 3 div 0]` ]] = ?
  - [[ `[1, 4, 2 div 0] !! 1` ]] = ?
  - [[ `2:3:[4,5]` ]] = ?
  - [[ `tail [4, 3 div 0, 7]` ]] = ?

## Funciones

- Tipo de una función
- Polimorfismo paramétrico
- Asociación de tipos:

  - `f x y z` =?= `((f x) y) z`
  - `f x y z` =?= `(f x y) z`
  - `f x y z` =?= `(f x) y z`
  - `f x y z` =?= `f (x y z)`
  - `f x y z` =?= `f (x y) z`
  - `f x y z` =?= `f x (y z)`

- Aplicaciones parciales

- Funciones de OS (HO functions)

  - `map`
  - `filter`
  - `concat`
  - `all`, `any`
  - `takeWhile`, `dropWhile`
  - `span`, `break`
  - `iterate`
  - `(.)`:

    - `(.):: (b -> c) -> (a -> b) -> (a -> c)`
    - `(.) f g x = f (g x) == f.g x`

  - `zipWith`

  - `flip`, `curry`, `uncurry`

  - `fold`, `foldr`, `foldl`

    - `foldr f e [] = e`
    - `foldr f e (x:xs) = f x (foldr f e xs)`
    - Escribir estas funciones por medio de foldr: `sum`, `product`, `and`, `or`, `length`, `concat`
    - Tail recursion (`foldl`) vs non-tail recursion (`foldr`)

## Let

- [[ `let <x> = <e> in <e'>` ]] = `e'[x/e]`
- Ámbito de ligaduras y defs locales
- [[ `let x=1:2:x in take 3 x` ]] = ?

## [=

- Definicion de `v [= v'`

## Lambda-expresiones

- `\x -> e`, `\x y -> e`, `\x -> \y -> e`
- [[ `(\x -> x+1) 2` ]] = ?
- [[ `(\x y -> x + y) 2 3` ]] = ?
- [[ `map (\x -> x+1) [2, 3, 4]` ]] = ?
- [[ `zipWith (\x y -> (x y))` ]] = ?
- Ajuste de patrones:

  - [[ `(\(x, y) -> x) (3, 4)` ]] = ?
  - [[ `(\ (x:xs) -> x) [1, 2]` ]] = ?
  - Explícito: `case`

## Listas Intensionales

- `[ e | g <, f> ]` donde

  - `e = expresion principal (e.g. e=x+1 )`
  - `g = generador (e.g. g=x<-[1..10])`
  - `f = filtro(s) (e.g. f=x<=10)`

- Orden de los generadores

- Ataduras de variables

- Vinculos de variables de fuera de la lista

  - `let u=3 in [u+x | x <- [1..u]]`

## Inferencia de Tipos

![](http://goo.gl/uJsEQ7)

## Tipos Construidos

- `data`
  - Naturales: `data Nat = Cero | Suc Nat`
    - `suma:: Nat -> Nat -> Nat`
    - `resta:: Nat -> Nat -> Nat`
    - `multiplicacion:: Nat -> Nat -> Nat`
    - `potencia:: Nat -> Nat -> Nat`
  - Listas: 'data List a = Nil | Cons a (List a)'
    - `[[ :t a]]` = ?
- `type` =~ el `typedef` de C
  - `type String `
  - `type Coordenada = Float`
  - `type Punto = (Coordenada, Coordenada)`
  - `distancia:: Punto -> Punto -> Float`
  - [[`:t distancia`]] = ?
  - [[`type T = (Int, [T])`]] = ?
  - [[`type Terna a = (a, a, a)`]] = ?

## Polimorfismo Ad-hoc

- `class`
- Clase de tipos = Colección de tipos + Métodos de Clase
- Se pueden ver como una Interfaz
- Ejemplo: Clase `Eq`, definición y uso
- `instance`
- `deriving`
- Subclases:
  - `class Eq a => Ord a where`
  - Para `data T = C1 ... | C2 ... | Cn ... deriving Ord`, `x, y en T`, calcular `x <= y`

- Ambigüedad de tipos
  - Con [[`:t toEnum`]] = `Enum a => a`:
    - [[`toEnum 0`]] = ?
    - [[`head [toEnum 0]`]] = ?
    - [[`head [toEnum 0, 1]`]] = ?

## I/O
![FTS](http://i.kinja-img.com/gawker-media/image/upload/s--jAvcarHK--/18wi95lz7ciuypng.png)
